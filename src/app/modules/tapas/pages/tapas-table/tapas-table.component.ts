import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TapasTableDataSource } from './tapas-table-datasource';
import {TapaModel} from '../../shared/tapa.model';
import {TapaService} from '../../shared/tapa.service';

@Component({
  selector: 'app-tapas-table',
  templateUrl: './tapas-table.component.html',
  styleUrls: ['./tapas-table.component.css'],
})
export class TapasTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TapasTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'likes', 'actions'];

  constructor(private tapaService: TapaService) { }

  ngOnInit() {
    this.tapaService.getTapas()
      .subscribe((tapas: Array<TapaModel>) => {
        this.dataSource = new TapasTableDataSource(this.paginator, this.sort, tapas);
      });
  }
  deleteTapa(id: string) {
   this.tapaService.deleteTapa(id)
     .finally(
       () => alert('Deleted!')
     );
  }
  likeTapa(tapa: TapaModel) {
    this.tapaService.likeTapa(tapa)
      .finally(
        () => alert('Thanks!')
      );
  }
}
