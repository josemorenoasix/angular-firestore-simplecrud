import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule, MatSortModule, MatTableModule } from '@angular/material';

import { TapasTableComponent } from './tapas-table.component';

describe('TapasTableComponent', () => {
  let component: TapasTableComponent;
  let fixture: ComponentFixture<TapasTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TapasTableComponent ],
      imports: [
        NoopAnimationsModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TapasTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
