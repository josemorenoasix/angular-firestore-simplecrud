import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {TapaModel} from '../../shared/tapa.model';
import {TapaService} from '../../shared/tapa.service';

@Component({
  selector: 'app-tapas-form',
  templateUrl: './tapas-form.component.html',
  styleUrls: ['./tapas-form.component.css'],
})
export class TapasFormComponent {
  tapasForm = this.fb.group({
    name: [null, Validators.compose([
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(30)]
    )],
    price: [0.00, Validators.compose([
      Validators.required,
      Validators.pattern('^\d+(,\d{1,2})?$'),
      Validators.min(0),
      Validators.max(5)]
    )],
  });

  constructor(private fb: FormBuilder, private tapaService: TapaService) {}

  onSubmit() {
    alert('Thanks!');
    this.tapaService.createTapa(new TapaModel(this.tapasForm.value));
  }
}
