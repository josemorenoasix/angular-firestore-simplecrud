import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, DocumentReference} from '@angular/fire/firestore';
import {TapaModel} from './tapa.model';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {LoggerService} from '../../../core/services/logger.service';

@Injectable({
  providedIn: 'root'
})
export class TapaService {
  private tapasCollection: AngularFirestoreCollection<TapaModel>;

  constructor(private afs: AngularFirestore) {
    this.tapasCollection = this.afs.collection<TapaModel>('tapas', (tapa) =>  {
      return tapa.orderBy('name', 'desc').orderBy('likes', 'desc');
    });
  }

  private static handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      LoggerService.log(`${operation} failed: ${error.message}`);

      if (error.status >= 500) {
        throw error;
      }

      return of(result as T);
    };
  }

  getTapas(): Observable<TapaModel[]> {
    return this.tapasCollection.snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map( (action) => {
            const data = action.payload.doc.data();
            return new TapaModel({id: action.payload.doc.id, ...data});
          });
        }),
        tap( () => LoggerService.log('fetch all tapas')),
        catchError(TapaService.handleError('getHeroes', []))
      );
  }

  getTapasPage(sort: string, order: 'desc' | 'asc', limit: number, index: string = null): Observable<any[]> {
    return this.afs.collection<TapaModel>('tapas', (tapa) => {
      return tapa.orderBy(sort, order).startAfter(index).limit(limit);
    }).snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map((action) => {
            const data = action.payload.doc.data();
            return new TapaModel({id: action.payload.doc.id, ...data});
          });
        }),
        tap(() => LoggerService.log('fetch paged tapas')),
        catchError(TapaService.handleError('getTapasPage', []))
      );
  }

  getTapa(id: string): Observable<any> {
    return this.afs.doc(`tapas/${id}`).get().pipe(
      map((tapa) => {
        return new TapaModel( {id, ...tapa.data()});
      }),
      tap(() => LoggerService.log(`fetched taoa ${id}`)),
      catchError(TapaService.handleError('getHero', []))
    );
  }

  createTapa(tapa: TapaModel): Promise<DocumentReference> {
    return this.tapasCollection.add(JSON.parse(JSON.stringify(tapa)))
      .then((document: DocumentReference) => {
        LoggerService.log(`added tapa w/ id=${document.id}`);
        return document;
      }, (error) => {
        TapaService.handleError<any>('createHero', error);
        return error;
      });
  }
  updateTapa(tapa: TapaModel): Promise<void> {
    return this.afs.doc(`tapas/${tapa.id}`)
      .update(JSON.parse(JSON.stringify(tapa)))
      .then(() => {
        LoggerService.log(`updated tapa w/ id=${tapa.id}`);
      });
  }

  deleteTapa(id: string): Promise<void> {
    return this.afs.doc(`tapas/${id}`)
      .delete()
      .then( () => {
        LoggerService.log(`deleted tapa w/ id=${id}`);
      });
  }
  likeTapa(tapa: TapaModel): Promise<void> {
    tapa.likes++;
    return this.afs.doc(`tapas/${tapa.id}`)
      .update(JSON.parse(JSON.stringify(tapa)))
      .then(() => {
        LoggerService.log(`liked tapa w/ id=${tapa.id}`);
      });
  }
}
