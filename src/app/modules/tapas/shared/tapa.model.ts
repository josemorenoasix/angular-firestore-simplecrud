import {Deserializable} from '../../../shared/intefaces/deserializable.interface';

export class TapaModel implements Deserializable {
  id: string;
  name: string;
  price: number;
  withDrink: boolean;
  likes: number;

  constructor(tapa: any = {}) {
    this.id = tapa.id;
    this.name = tapa.name || '';
    this.likes = tapa.likes || 0;
    this.withDrink = tapa.withDrink || false;
    this.price = tapa.price || 0;
  }

  like() {
    this.likes += 1;
    localStorage.setItem('votes', '' + (Number(localStorage.getItem('votes')) + 1));
  }

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }

}
