import { TestBed } from '@angular/core/testing';

import { TapaService } from './tapa.service';

describe('TapaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TapaService = TestBed.get(TapaService);
    expect(service).toBeTruthy();
  });
});
