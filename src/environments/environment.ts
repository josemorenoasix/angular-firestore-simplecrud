// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCPmsjHDhS8u7Fr5kK8xEXSYD1Hji9RtnM',
    authDomain: 'a7-simple-crud.firebaseapp.com',
    databaseURL: 'https://a7-simple-crud.firebaseio.com',
    projectId: 'a7-simple-crud',
    storageBucket: 'a7-simple-crud.appspot.com',
    messagingSenderId: '520306281958'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
